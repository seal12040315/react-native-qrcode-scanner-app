import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { Root } from 'native-base';
// screens
import Home from '../screens/Home.js'
import Scan from '../screens/Scan.js'
import Capture from '../screens/Capture.js'

const AppNavigator = createStackNavigator({
    Home: {
        screen: Home
    },
    Scan: {
        screen: Scan
    },
    Capture: {
        screen: Capture
    }
},  {
    initialRouteName: 'Home',
    headerMode: "none"
});

const CreateAppContainer = createAppContainer(AppNavigator)
class AppContainer extends React.Component {
    render() {
        return (
            <Root>
                <CreateAppContainer/>   
            </Root>
        )
    }
}

export default AppContainer