import React from 'react'
import { withNavigationFocus } from 'react-navigation' 
import {
    View,
    TouchableOpacity,
} from 'react-native'
import CameraRoll from "@react-native-community/cameraroll";
import {
    Container,
    Header,
    Left,
    Button,
    Icon,
    Body,
    Title,
    Right,
} from 'native-base'
import { RNCamera } from 'react-native-camera'

class Capture extends React.Component {
    constructor(props) {
        super(props)
    }

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true }
            const data = await this.camera.takePictureAsync(options);
            CameraRoll.saveToCameraRoll(data.uri).then(res => {
                console.log(res);
            });
        }
    }

    render() {
        const {isFocused} = this.props;
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Capture for PDF</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                            <Icon name="menu"/>
                        </Button>
                    </Right>
                </Header>
                {
                    isFocused && 
                    <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'black'}}>
                        <RNCamera 
                            ref={ref => {this.camera = ref}}
                            type={RNCamera.Constants.Type.back}
                            flashMode={RNCamera.Constants.FlashMode.on}
                            androidCameraPermissionOptions={{
                                title: 'Permission to use camera',
                                message: 'We need your permission to use your camera',
                                buttonPositive: 'Ok',
                                buttonNegative: 'Cancel'
                            }}
                            captureAudio={false}
                            style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}
                        /> 
                        <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={this.takePicture.bind(this)} style={{position: 'absolute', backgroundColor: '#fafafa', borderRadius: 15, padding: 10, paddingHorizontal: 12, alignSelf: 'center', bottom: 30}}>
                                <Icon name="camera" style={{flex: 0}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
            </Container>
        )
    }
}

export default withNavigationFocus(Capture)