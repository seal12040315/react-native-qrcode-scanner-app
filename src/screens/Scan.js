import React, { Component } from 'react';
import { withNavigationFocus } from 'react-navigation'
import {
  Dimensions, 
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
} from 'native-base'
const screenHeight = Math.round(Dimensions.get('window').height)

class Scan extends Component {
  constructor(props) {
    super(props);
  }
  onSuccess = (e) => {
    alert(JSON.stringify(e));
    this.props.navigation.push('Capture'); 
  }

  render() {
      const { isFocused } = this.props;
      return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name="arrow-back"/>
                    </Button>
                </Left>
                <Body>
                    <Title>Scanning</Title>
                </Body>
                <Right>
                    <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
                        <Icon name="menu"/>
                    </Button>
                </Right>
            </Header>
            {
                isFocused &&
                <Content>
                    <QRCodeScanner
                        containerStyle={{height: (screenHeight - 75)}}
                        cameraStyle={{height: (screenHeight - 75)}}
                        reactivate={false}
                        showMarker={true}
                        onRead={this.onSuccess}
                    /> 
                </Content>
            }
        </Container>
    );
  }
};


export default withNavigationFocus(Scan);
