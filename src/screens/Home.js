import React from 'react'
import { Image, View } from 'react-native'
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Title,
    Button,
    Icon,
    Content,
    Footer,
    H1,
    Text } from 'native-base'
import logo from '../assets/images/logo.jpeg';
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description : "QR code (abbreviated from Quick Response Code) is the trademark for a type of matrix barcode (or two-dimensional barcode) first designed in 1994 for the automotive industry in Japan. In practice, QR codes often contain data for a locator, identifier, or tracker that points to a website or application."
        }
    }
    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Home</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='menu'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', padding: 10}}>
                        <Image style={{width: 200, height: 100, borderBottomLeftRadius: 30,borderBottomRightRadius: 0,borderTopRightRadius: 30}} source={logo} resizeMode="center"/>
                    </View>
                    <View style={{padding: 10}}>
                        <H1 style={{textAlign: 'center'}}>Notebook QR Code Scanning</H1>
                        <Text style={{textAlign: 'center', padding: 10}}>
                            {this.state.description}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'center', margin: 10}}>
                            <Button onPress={() => this.props.navigation.push('Scan')}>
                                <Text>Scanning Start</Text>
                            </Button>
                        </View>
                    </View>
                </Content>
            </Container>
        )
    }
}

export default Home;